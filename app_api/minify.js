var uglifyJs = require("uglify-js");
var fs = require('fs');

var appClientFiles = [ 
    'app_client/app.js', 
    
    'app_client/home/home.controller.js',
    
    'app_client/common/services/cbcData.service.js',
    
    'app_client/common/directives/footerGeneric/footerGeneric.directive.js',
    'app_client/common/directives/navbarGeneric/navbarGeneric.directive.js',
    'app_client/common/directives/navbarList/navbarList.directive.js',
    'app_client/common/directives/navbarList/navigationList.controller.js',
    'app_client/common/directives/dropdownList/dropdownCtrl.controller.js',
    'app_client/common/directives/dropdownList/dropdown.directive.js',
    'app_client/common/directives/contacts/contacts.directive.js',
    'app_client/common/directives/trainers/trainers.directive.js',
    'app_client/common/directives/trainers/trainee.controller.js',
    'app_client/slides_carousel/carousel.controller.js',
    'app_client/slides_carousel/slide_carousel.directive.js',
    'app_client/common/directives/inlineSlidesServices/inlineSlidesServices.controller.js',
    'app_client/common/directives/inlineSlidesServices/inlineSlidesServices.directive.js',
    'app_client/training_start_form/training_start_form.controller.js'
    
];

//    

var create_from_files = [];

for(var i=0; i < appClientFiles.length; i++){
    create_from_files.push(fs.readFileSync(appClientFiles[i], 'utf8'));
}

var uglified = uglifyJs.minify(create_from_files, { compress : false });

fs.writeFile('public/javascripts/cbc.min.js', uglified.code, function (err){
    if(err) { 
        console.log(err); 
    } else { 
        console.log('Script generated and saved: cbc.min.js'); 
    } 
});
