var mongoose = require( 'mongoose' );
require('./schemas'); //js to work with db

var dbURI = 'mongodb://127.0.0.1:27017/cbc';

if (process.env.NODE_ENV === 'production')
    dbURI = process.env.MONGOLAB_URI;

mongoose.connect(dbURI);


mongoose.connection.on('connected', function () { 
console.log('Mongoose connected to ' + dbURI); 
}); 
mongoose.connection.on('error',function (err) { 
console.log('Mongoose connection error: ' + err); 
}); 
mongoose.connection.on('disconnected', function () { 
console.log('Mongoose disconnected'); 
});

var gracefulShutdown = function (msg, callback) { 
    mongoose.connection.close(function () { 
    console.log('Mongoose disconnected through ' + msg); 
    callback(); 
    });
};
//nodemon event
process.once('SIGUSR2', function () { 
    gracefulShutdown('nodemon restart', function () { 
    process.kill(process.pid, 'SIGUSR2'); 
    });
});
//windows event
process.on('SIGINT', function () { 
    gracefulShutdown('app termination', function () { 
    process.exit(0); 
    });
});
//for heroku
process.on('SIGTERM', function() { 
    gracefulShutdown('Heroku app shutdown', function () { 
    process.exit(0); 
    });
});




