
(function () {
angular.module('app_Angular', ["ngRoute", "ui.bootstrap", 'ngAnimate', 'ngSanitize']);
//translate directive to mark
angular
    .module('app_Angular')
    .config(['$routeProvider', "$locationProvider", config]);
    
function config ($routeProvider, $locationProvider) { 
    $routeProvider 
        .when('/', { 
            templateUrl: 'home/home.view.html',
            controller: 'homeCtrl',
            controllerAs: 'vm'
        }) 
        .when('/about', { 
            templateUrl: '/common/about/genericText.view.html',
            controller: 'aboutCtrl',
            controllerAs: 'vm'
        })
        .otherwise({
            redirectTo: '/'
        }); 
    $locationProvider.html5Mode(true);
}; 
})();
