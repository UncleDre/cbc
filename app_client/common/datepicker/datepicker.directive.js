(function () {

angular 
    .module('app_Angular') 
    .directive('datePicker', datePicker);
    
function datePicker() { 
   return {
       restrict: 'E',
       templateUrl: '/common/datepicker/datepicker.template.html'
   };
  };
})();


