
(function () {
    angular
        .module('app_Angular')
        .directive('contacts', contacts);

    function contacts () {
        return {
            restrict: 'EA',
            templateUrl: '/common/directives/contacts/contacts.template.html'
        };
    }
})();