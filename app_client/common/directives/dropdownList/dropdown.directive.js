
(function () {
    angular
        .module('app_Angular')
        .directive('dropdownList', dropdownList);

    function dropdownList () {
        return {
            restrict: 'EA',
            templateUrl: '/common/directives/dropdownList/dropdown.template.html'
        };
    }
})();