(function () {

ourServicesCtrl.$inject = ['$scope'];

angular 
    .module('app_Angular') 
    .controller('ourServicesCtrl', ourServicesCtrl);
    
function ourServicesCtrl($scope) { 
    $scope.services = [{
          name:         "Crossfit",
          img_src:      '/images/services_sliders/1.jpg',
          features:     ["Stonger","Nice","Smarter"],
          id:           1
        },{
          name:         "BikeRider",
          img_src:      '/images/services_sliders/2.jpg',
          features:     ["Stonger","Outstanding","Grow"],
          id:           2 
        },{
          name:         "Youga",
          img_src:      '/images/services_sliders/3.jpg',
          features:     ["Stonger","Poophy","Slow"],
          id:           3  
        }];
  };
})();


