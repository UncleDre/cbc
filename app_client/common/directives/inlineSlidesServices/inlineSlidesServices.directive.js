
(function () {
    angular
        .module('app_Angular')
        .directive('servicesInlineSlides', servicesInlineSlides);

    function servicesInlineSlides () {
        return {
            restrict: 'EA',
            templateUrl: '/common/directives/inlineSlidesServices/inlineSlidesServices.template_1.html'
        };
    }
})();