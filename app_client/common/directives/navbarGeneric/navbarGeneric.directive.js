
(function () {
    angular
        .module('app_Angular')
        .directive('navbarGeneric', navbarGeneric);

    function navbarGeneric () {

        return {
            restrict: 'EA',
            templateUrl: '/common/directives/navbarGeneric/navbarGeneric.template.html',
            scope: {
                pages: '@'
            }
        };
    }
})();