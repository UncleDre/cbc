
(function () {
    angular
        .module('app_Angular')
        .directive('navbarSelectList', navbarSelectList);

    function navbarSelectList () { 
        return {
            restrict: 'EA',
            templateUrl: '/common/directives/navbarList/navbarList.template.html'
        };
    }
})();