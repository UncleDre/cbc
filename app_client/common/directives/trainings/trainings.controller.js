
(function () {

ourDisciplinesCtrl.$inject = ['cbcData','$uibModal', "$routeParams"];

angular 
    .module('app_Angular') 
    .controller('ourDisciplinesCtrl', ourDisciplinesCtrl);
    
function ourDisciplinesCtrl(cbcData, $uibModal, $routeParams) {  
    var vm = this;
    vm.trainings = [{
          name:         "Crossfit",
          img_src:      '/images/trainings/tr_1.jpg',
          features:     ["Stonger","Nice","Smarter"],
          details_id:   "Crossfit_regist",
          id:           1
        },{
          name:         "BikeRider",
          img_src:      '/images/trainings/tr_2.jpg',
          features:     ["Stonger","Outstanding","Grow"],
          details_id:   "BikeRider-regist",
          id:           2 
        },{
          name:         "Youga",
          img_src:      '/images/trainings/tr_2.jpg',
          features:     ["Stonger","Poophy","Slow"],
          details_id:   "Youga-regist",
          id:           3  
        }];
    
    vm.popupTrainingForm = function(){
        console.log("participate btn pressed ");
        var modalInstance = $uibModal.open({
           templateUrl: "/training_start_form/training_start_form.view.html",
           controller: 'start_trainingModalCtrl as vm'
        });

        modalInstance.result.then(function (data) {
            console.log("the promise from modal is resolved, we have data: ", data.data);
            //vm.data.location.reviews.push(data.data);
        },  function () {
            console.log("the promise from modal is resolved second Callback ");
        });
        };
    };
})();