
(function () {
    angular
        .module('app_Angular')
        .directive('trainingsPages', trainingsPages);

    function trainingsPages () {
        return {
            restrict: 'EA',
            templateUrl: '/common/directives/trainings/trainings.template.html'
        };
    }
})();