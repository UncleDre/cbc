(function () {

cbcData.$inject = ['$http'];

angular
    .module('app_Angular')
    .service('cbcData',  cbcData);

function cbcData ($http) {
    var addTrainingReq = function (data) {
        console.log("Training reqest added with credentials", data);
        return $http.post('/api/trainings/' + data.trainingName + '/new_comer', data);
    };
    /* 
    var locationByCoords = function () {
        var lng = -0.9690819;
        var lat = 51.455043;
        return $http.get('/api/locations?lng=' + lng + '&lat=' + lat + 
        '&maxDistance=2000');
        };
    var locationById = function (locationid) {
        return $http.get('/api/locations/' + locationid);
    };
    
    var addReviewById = function (locationid, data) {
        return $http.post('/api/locations/' + locationid + '/reviews', data);
    };
    */
    return {
        //locationByCoords : locationByCoords,
        //locationById : locationById,
        //addReviewById : addReviewById
        addTrainingReq : addTrainingReq
    };
    

};
})(); 
