
var nav_btn_change_visibility = function(target_id){  
    var obj = document.getElementById(target_id);
    var pressed = false;
    //console.log("btn_change_visibility : ", obj.classList);
    for(var i=0; i < obj.classList.length; i++)
    {
        if (obj.classList[i] === "in")
            pressed = true;
    }
    if (!pressed)    {
        obj.classList.add("collapse");
        obj.classList.add("in");
        console.log("become pressed : ", target_id);
    }
    else {
        obj.classList.add("collapse");
        obj.classList.remove("in"); 
        console.log("not pressed : ", target_id);
    }
};

var scroll_to = function(element_id){
    var navbar_offset = 120;
    if(element_id) {
        var element     = document.getElementById(element_id);
        var rect        = element.getBoundingClientRect();
        var bodyRect    = document.body.getBoundingClientRect();
        var offset      = rect.top - bodyRect.top - navbar_offset ;
        console.log("element : ", element);
        console.log("go to : ", offset);
        //setTimeout(function() {window.scrollTo(0, offset);},1);
        window.scrollTo(0, offset);
    }
    else{
        console.log("no element_id parameter : ", element_id);
        //show error msg
    }
        
};




