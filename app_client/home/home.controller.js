(function () {

//homeCtrl.$inject = ['cbcData'];

angular 
    .module('app_Angular') 
    .controller('homeCtrl', homeCtrl);
    
function homeCtrl() { 
    var vm = this;
    vm.pageHeader = { 
        title: 'cbc', 
        strapline: 'Find places to work with wifi near you!' 
        }; 
    vm.page_list = [{
            name: "page 1",
            id: 2,
            url:  "/" },{
            name: "page 2",
            id: 1,
            url:  "/" },{
            name: "page 3",
            id: 4,
            url:  "/" }];
};
})();    