
(function () {
    angular
        .module('app_Angular')
        .directive('slideCarousel', slideCarousel);

    function slideCarousel () {
        return {
            restrict: 'EA',
            templateUrl: '/slides_carousel/carousel.view.html'
        };
    }
})();