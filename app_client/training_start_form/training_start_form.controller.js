(function () {

  angular
    .module('app_Angular')
    .controller('start_trainingModalCtrl', start_trainingModalCtrl);

  start_trainingModalCtrl.$inject = ['$uibModalInstance', 'cbcData'];
  
  function start_trainingModalCtrl ($uibModalInstance, cbcData) {
    var vm = this;
    //vm.trainingData = trainingData;
    vm.trainingData = {
        trainingName: "crossfit POWER UP"
    };
    
    vm.onSubmit = function () {
      vm.formError = "";
      if (!vm.formData.name || !vm.formData.phone) {
        vm.formError = "All fields required, please try again";
        return false;
      } else {
        vm.doAddTrainingReqest(vm.formData, vm.trainingData.trainingName);
      }
    };

    vm.doAddTrainingReqest = function (formData, trainingName) {
      cbcData.addTrainingReq( {
        sportsman:      formData.name,
        trainingName:       trainingName,
        credentials: {
            name:       formData.name,
            phone:      formData.phone,
            email:      formData.email
        }
        })
        .then(function successCallback(data) {
          vm.modal.close(data);
        }, 
            function errorCallback(data) {
          vm.formError = "Your ask has not been saved, please try again";
        });
      return false;
    };
    
    vm.get_training_name = function() {
        return vm.trainingData.trainingName;
    };

    vm.modal = {
      close : function (result) {
        $uibModalInstance.close(result);
      },
      cancel : function () {
        $uibModalInstance.dismiss('cancel');
      }
    };

  }

})();